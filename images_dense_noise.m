function [Iseq]=images_dense_noise(imageSize,imageBorder,gridSize,numImages)
if rem(imageSize,gridSize)~=0, 
  disp(['ERROR image_m_sequence: ' ...
		'please make image size an exact multiple of the grid size']);
end

%create a sequence of images, in which each image is created such that each
%pixel is randomly assigned a value of 0 or 1
pixelsPerSquare=imageSize/gridSize;

for t=1:numImages
  %create blank image with border that will surround checkerbord pattern
  Iseq(:,:,t)=zeros(imageSize+2*imageBorder,'single')+0.5;
  for locationX=1:gridSize
	for locationY=1:gridSize
	  %fill in one square of the checkerboard with the value determined by
      %the random number generator
	  xStart=1+imageBorder+(locationX-1)*pixelsPerSquare;
	  yStart=1+imageBorder+(locationY-1)*pixelsPerSquare;
	  xEnd=xStart+pixelsPerSquare-1;
	  yEnd=yStart+pixelsPerSquare-1;
	  Iseq(xStart:xEnd,yStart:yEnd,t)=randi([0,1]);
	end
  end
end

