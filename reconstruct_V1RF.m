function RFrecon=reconstruct_V1RF(w,sigma)
[n,c]=size(w);
LoG=filter_definitions_LGN(sigma);

for j=1:n, 
  %convolve the ON and OFF weights with the LoG filter
  RFon=conv2(w{j,1},LoG,'same');
  RFoff=conv2(w{j,2},-LoG,'same');

  %save reconstructd values
  RFrecon{j}=RFon+RFoff;
end
