function [binned_param_values,bin_labels]=binned_param(param_vals,range,num_bins)

step=(max(range)-min(range))./num_bins;
if step==0;
  binned_param_values=ones(size(param_vals));
  bin_labels=max(param_vals);
else
  
  k=0;
  for lowerval=min(range):step:max(range)
	k=k+1;
	if k==1	  
	  to_bin=find(param_vals<lowerval+step);
	elseif k==num_bins
	  to_bin=find(param_vals>=lowerval);
	else
	to_bin=find(param_vals>=lowerval & param_vals<lowerval+step);
	end
	binned_param_values(to_bin)=min(k,num_bins);
	bin_labels(k)=lowerval+0.5*step;
  end
  binned_param_values(isnan(param_vals))=NaN;
end
bin_labels=bin_labels(1:num_bins);