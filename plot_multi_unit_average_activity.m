function plot_multi_unit_average_activity(correl,plotNodes)
numNodes=length(correl);
[numStim,numTimes]=size(correl{1})
gridSize=sqrt(numStim/2)

%calculate multi-unit average acvitivy
yMUA=mean(cat(3,correl{plotNodes}),3);
  size(yMUA)
  
maxResp=max(max(yMUA))
clf
k=0;
for locationX=1:gridSize
  for locationY=1:gridSize
	k=k+1;
	maxsubplot(gridSize,gridSize,k);
	plot(yMUA(k*2-1,:),'k');
	hold on
	plot(yMUA(k*2,:),'g');
	axis([1,numTimes,0,maxResp])
	set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end