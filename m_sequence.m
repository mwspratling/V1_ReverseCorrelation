function mseq=m_sequence(order, tap)
register=randi([0,1],1,order,'single');
tap=single(dec2bin(tap,order)-48);

%test with Reid etal example:
%register=[0,0,0,1]; tap=[0,0,1,1]; order=4;

for i=1:2^order-1
  mseq(i)=register(order);
  new=parity(register & tap);
  register=[new,register(1:order-1)];
end



function p=parity(X)
p=mod(sum(X),2);
