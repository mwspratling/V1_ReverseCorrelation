function Iseq=images_sparse_noise(imageSize,imageBorder,gridSize,barLen)
%Returns a sequence of images, made up of every possible combination of a
%single grid square or rectangle at a contrast of 0 or 1 on a uniform
%background of contrast 0.5.
%
%imageSize =  the size of the checkerboard image (pixels)
%imageBorder = the width of the blank area around the checkerbord image (pixels)
%gridSize = the number of rows and columns of squares making up the checkerboard
%barLen = the length of the bar making up each pattern (optional, default=1)
if rem(imageSize,gridSize)~=0, 
  disp(['ERROR image_rectangular_bar: ',...
		'please make image size an exact multiple of the grid size']);
end

if nargin<4
  barLen=1;
end

pixelsPerSquare=imageSize/gridSize;

t=0;
for locationX=1:gridSize
  for locationY=1:gridSize+1-barLen
	for luminance=0:1
	  t=t+1;
	  %create blank image with border that will surround checkerbord pattern
	  Iseq(:,:,t)=zeros(imageSize+2*imageBorder,'single')+0.5;
	  %fill in one square/rectangle of the checkerboard
	  xStart=1+imageBorder+(locationX-1)*pixelsPerSquare;
	  yStart=1+imageBorder+(locationY-1)*pixelsPerSquare;
	  xEnd=xStart+pixelsPerSquare-1;
	  yEnd=yStart+barLen*pixelsPerSquare-1;
	  Iseq(xStart:xEnd,yStart:yEnd,t)=luminance;
	end
  end
end



