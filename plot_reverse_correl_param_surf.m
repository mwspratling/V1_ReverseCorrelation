function plot_reverse_correl_param_surf(w,correl,bin_labels,plotNodes)
if nargin<4
  numNodes=length(correl);
else
  numNodes=length(plotNodes);
end
[poo,numTimes]=size(correl{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
plotTimes=[-maxTime:maxTime];

numBins=length(bin_labels);
correlMax=max(max(max(cat(3,correl{:}))));

nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  maxsubplot(numNodes+1,2,2+2*nodeNum);
  imagesc(correl{node})
  drawnow;
  if nodeNum~=1
	set(gca,'XTick',[]);
  end

  if node<=maxNodes
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,2,1+2*nodeNum);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end