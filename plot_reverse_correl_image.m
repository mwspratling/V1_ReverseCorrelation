function plot_reverse_correl_image(w,rcImage,imageBorder,plotNodes,plotTimes,surf)
if ~isempty(w), 
  wRecon=reconstruct_V1RF(w,1.5); 
  colPlus=1; 
else
  colPlus=0;
end
if nargin<4
  numNodes=length(rcImage);
else
  numNodes=length(plotNodes);
end
[a,b,numTimes]=size(rcImage{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<5
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end
if nargin<6, surf=0; end

clf

nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the RF reconstructed from the reverse correlation at
  %each time lag
  correl=rcImage{node}(imageBorder+1:a-imageBorder,imageBorder+1:b-imageBorder,:);
  lim=max(1e-9,0.9.*max(max(max(abs(correl)))));

  
  toff=0;
  for time=plotTimes
	toff=toff+1;
	maxsubplot(numNodes,colPlus+numTimes,(colPlus+numTimes)*(nodeNum-1)+toff+colPlus);
	if surf
	  contourf(correl(:,:,time+maxTime+1),[-lim:lim/3:lim],'-');caxis([-lim,lim])
	else
	  imagesc(correl(:,:,time+maxTime+1),[-lim,lim]);
	end
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;	
  
    spatialVariance(toff)=spatial_variance(correl(:,:,time+maxTime+1));
  end
  spatialVarianceNoise=spatial_variance(correl(:,:,1));
  
  if node<=maxNodes
	%show corresponding synaptic weigths, padded so that weights are same scale as
	%the images
	n_actual=zeros(a,b);
	[ag,bg]=size(w{1,1});
	n_actual(ceil(a/2)-floor(ag/2):ceil(a/2)+floor(ag/2),ceil(b/2)-floor(bg/2):ceil(b/2)+floor(bg/2))=wRecon{node};%w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(n_actual))));
	maxsubplot(numNodes,colPlus+numTimes,(colPlus+numTimes)*(nodeNum-1)+1);
	imagesc(n_actual(imageBorder+1:a-imageBorder,imageBorder+1:b-imageBorder),[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end
cmap=colormap('gray');
cmap=1-cmap;
colormap(cmap); 
set(gcf,'PaperPosition',2.*[1 1 numTimes+colPlus numNodes]);

%if maxNodes>0
%  set(gcf,'PaperPosition',2.*[1 1 numTimes numNodes]);
%else
%  set(gcf,'PaperPosition',2.*[1 1 numTimes+colPlus numNodes]);
%end

if numTimes>0
  spatialVariance
  snr=spatialVariance./spatialVarianceNoise
end

function var=spatial_variance(vals)
vals=vals(:);
var=mean( (vals-mean(vals)).^2 );
