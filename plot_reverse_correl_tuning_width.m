function plot_reverse_correl_tuning_width(w,rcOrient,bin_labels,plotNodes,plotTimes)
if ~isempty(w), 
  wRecon=reconstruct_V1RF(w,1.5); 
  colPlus=1; 
else
  colPlus=0;
end
if nargin<4
  numNodes=length(rcOrient);
else
  numNodes=length(plotNodes);
end
[poo,numTimes]=size(rcOrient{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<5
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end
numBins=size(rcOrient{1},1);
binsToLabel=[1:floor(numBins/2):numBins];
rcOrientMax=max(max(max(cat(3,rcOrient{:}))))

angleStep=180./numBins;
angle_values=[0:angleStep:180-angleStep]

disp('thicker lines = earlier times');
clf
nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  toff=0;
  maxsubplot(numNodes+1,colPlus+1,(colPlus+1)*(nodeNum+colPlus));
  
  for time=plotTimes
	toff=toff+1;
	data=rcOrient{node}(:,time+maxTime+1);
	
	data=(data-min(data))./(max(data)-min(data));
	plot(data,'LineWidth',1+length(plotTimes)-toff), 
	axis([0,numBins+1,0,1]), hold on
	drawnow;
	if nodeNum==1
	  set(gca,'XTick',binsToLabel,'XTickLabel',num2str(bin_labels(binsToLabel)')); 
	else
	  set(gca,'XTick',[]);
	end
	
	%rotate orientation tuning curves so that orientation preference is approx
	%central: this will allow a Gaussian to be fitted to the data.
	numOris=size(w,1)/4;
	cent=5; %1st node with a central orientation preference
	oriVals=zeros(numNodes,numBins);
	
	ori=mod(node-1,numOris)+1;
	cut=fix((cent-ori)*numBins/numOris);
	if cut>0
	  range1=numBins-cut+1:numBins;
	  range2=1:numBins-cut;
	elseif cut<0
	  cut=abs(cut);
	  range1=cut+1:numBins;
	  range2=1:cut;
	end	
	if cut~=0
	  %centre data
	  dataCentred(1:length(range1),:)=data(range1,:);
	  dataCentred(length(range1)+1:numBins,:)=data(range2,:);
	else
	  dataCentred=data;
	end
	%plot(dataCentred,'g')
	
	[A, mu, sigma, error] = fitgauss(data,angle_values');
	%plot(A*exp(-0.5*((angle_values-mu)./sigma).^2)/sqrt(2*pi*sigma^2),'m');
	if sigma>0 & error<0.1  	  
	  prefSigma(node,toff)=sigma; 
	else
	  prefSigma(node,toff)=NaN;
	end
  end
  if node<=maxNodes
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,2+numTimes,(numTimes+2)*(nodeNum)+1);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end 
end
prefSigma
prefSigma(prefSigma==0)=NaN;
nanmean(prefSigma,1)