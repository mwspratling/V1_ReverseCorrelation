function plot_reverse_correl_orient(w,rcOrient,bin_labels,plotNodes,plotTimes)
colPlus=1; 
if nargin<4
  numNodes=length(rcOrient);
else
  numNodes=length(plotNodes);
end
[poo,numTimes]=size(rcOrient{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<5
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end

numBins=size(rcOrient{1},1);
%binsToLabel=[1:floor(numBins/2):numBins+1];
binsToLabel=[floor(numBins/4),1+ceil(numBins/2),2+ceil(numBins*0.75)];

clf
nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  toff=0;
  for time=plotTimes
	toff=toff+1;
	maxsubplot(numNodes+1,colPlus+numTimes,(colPlus+numTimes)*(nodeNum)+toff+colPlus);
	data=rcOrient{node}(:,time+maxTime+1);
	data=data./sum(data); %Ringbach et al 1997

	plot(data,'LineWidth',2), axis([1,numBins+1,0.05,0.08])
	drawnow;
	if nodeNum==1 & ~isempty(bin_labels)
	  set(gca,'XTick',binsToLabel,'XTickLabel',num2str(bin_labels(binsToLabel)'));
	else
	  set(gca,'XTick',[]);
	end
	if toff==1
	  set(gca,'YTick',[0:0.01:0.15]);
	  ylabel('p(\tau,\theta) / \Sigma_\theta p(\tau,\theta)','FontSize',12)
	else
	  set(gca,'YTick',[]);
	end
	if nodeNum==1 & toff==ceil(numTimes/2)
	  xlabel('Orientation (degrees)','FontSize',12)
	end
	set(gca,'FontSize',10);
  end

  if node<=maxNodes
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,2+numTimes,(numTimes+2)*(nodeNum)+1);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end

set(gcf,'PaperPosition',2.*[1 1 numTimes 2*numNodes]);
