function [w,rcImage]=V1_reverse_correl_m_seq()

sigmaV1=4;
sigmaLGN=1;
plotResults=1;
plotNodes=[1:5,33];%[1:8:32,3:8:32];

imageBorder=13; %number of blank pixels surrounding image
imageSize=32;
presentationTime=3;
numTrials=1;
gridSize=16;

%define v1 prediction node receptive fields
w=filter_definitions_V1_simple(sigmaV1);

%CREATE THE IMAGE DATA SET
disp('generating msequence')

order=16;
tap=57;
mseq=m_sequence(order, tap);
numImages=length(mseq)


%PERFORM REVERSE CORRELATION EXPERIMENT
y=[];
for node=1:40
  rcImage{node}=zeros(imageSize+2*imageBorder,imageSize+2*imageBorder);
end
disp('calculating reverse correlation for time-lag zero')
for t=1:numImages
  %generate next image in sequence
  Iraw=image_m_sequence(imageSize,imageBorder,gridSize,mseq,order,t);
  Iraw=single(Iraw);

  %pre-process image to generate input to V1 model
  X=preprocess_V1_input(Iraw,sigmaLGN);
  
  %Present images to V1 model and record response
  [y,r,e,ytrace]=dim_activation_conv(w,X,presentationTime,y);
  numSimple=length(ytrace);
  %calculate the response of complex cells by taking the maximum over the
  %outputs of the 4 simple cells with same orientation
  for complex=1:8
	ytrace{numSimple+complex}=calc_resp_complex(ytrace,1,1,complex+[0:8:31]);
  end
  
  %record correlation at time-lag zero
  for node=1:length(ytrace)
	for k=1:presentationTime
	  rcImage{node}=rcImage{node}+ytrace{node}(k).*(Iraw-0.5); 
	end
  end
  
  if plotResults & rem(t,1000)==0
	t
	max(max(abs(rcImage{1})))
	disp('plotting reverse correlations')
	figure(1), clf
	plot_reverse_correl_image(w,rcImage,imageBorder,plotNodes);
  end
end









