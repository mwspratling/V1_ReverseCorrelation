function plot_reverse_correl_joint_orient(w,rcJointOrient,bin_labels,plotNodes,plotTimes)
if nargin<4
  numNodes=length(rcJointOrient);
else
  numNodes=length(plotNodes);
end
[poo,poo,numTimes]=size(rcJointOrient{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<5
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end

numBins=length(bin_labels);
binsToLabel=[1:floor(numBins/2):numBins];

clf
nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  toff=0;
  maxData=0;
  for time=plotTimes
	toff=toff+1;
	maxsubplot(numNodes+1,1+numTimes,(numTimes+1)*(nodeNum)+toff+1);
	data=rcJointOrient{node}(:,:,time+maxTime+1);
	imagesc(data)
	axis('equal','tight');
	drawnow;
	if nodeNum==1
	  set(gca,'XTick',binsToLabel,'XTickLabel',num2str(bin_labels(binsToLabel)')); 
	else
	  set(gca,'XTick',[]);
	end
	if toff==1
	  set(gca,'YTick',binsToLabel,'YTickLabel',num2str(bin_labels(binsToLabel)')); 
	else
	  set(gca,'YTick',[]);
	end
  end

  if node<=maxNodes & ~isempty(w)
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,2+numTimes,(numTimes+2)*(nodeNum)+1);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end