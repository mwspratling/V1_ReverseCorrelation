function plot_reverse_correl_freq_logprob(w,rcFreq,rcBlank,bin_labels,plotNodes,plotTimes,together)
colPlus=1; 
if nargin<5
  numNodes=length(rcFreq);
else
  numNodes=length(plotNodes);
end
[poo,numTimes]=size(rcFreq{1});
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<6
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end
if nargin<7, together=0; end

numBins=size(rcFreq{1},1);
%binsToLabel=[1:floor(numBins/2):numBins+1];
binsToLabel=[floor(numBins/4),2+ceil(numBins*0.75)];

lineStyle=['b- ';'r--';'g-.';'c. '];
styleNum=1;

meanA=0;
meanRmax=0;
meanRmin=0;
meanRorth=0;
clf
nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  toff=0;
  maxData=0;
  for time=plotTimes
	toff=toff+1;

	data=rcFreq{node}(:,time+maxTime+1);
	%blank=rcBlank{node}(time+maxTime+1);
	%blank=mean(rcFreq{node}(:,time+maxTime+1));
	tblank=plotTimes(1)+maxTime+1;
	blank=rcFreq{node}(:,tblank);
	
	%calculate R values (the "tuning curve") and parameters describing this curve,
    %as used by Ringach et al
	R(:,toff)=log10(data./blank);

	%plot tuning curve a current time step
	if together
	  if nodeNum==1
		axh(toff)=maxsubplot(2,numTimes+1,(numTimes+1)+1+toff);
	  else
		axes(axh(toff));
		styleNum=rem(nodeNum-1,length(lineStyle))+1;
	  end
	else
	  maxsubplot(numNodes+1,colPlus+numTimes+2,(colPlus+numTimes+2)*(nodeNum)+toff+colPlus);
	end
	plot(R(:,toff),lineStyle(styleNum,:),'LineWidth',2), 
	axis([0,numBins+3,-1,0.5])
	hold on, plot([0,numBins+5],[0,0],'k')
	drawnow;
	if (nodeNum==1 | together) & ~isempty(bin_labels)
	  set(gca,'XTick',binsToLabel,'XTickLabel',num2str(bin_labels(binsToLabel)',2));
	else
	  set(gca,'XTick',[]);
	end
	if toff==1
	  set(gca,'YTick',[-1:0.5:1]);
	  ylabel('FT(\tau,f)','FontSize',12)
	else
	  set(gca,'YTick',[]);
	end
	if nodeNum==1 & toff==ceil(numTimes/2)
	  xlabel('Spatial Frequency (cycles/pixel)','FontSize',12)
	end
	set(gca,'FontSize',10);
  end

  if node<=maxNodes & ~isempty(w) & ~together
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,2+numTimes,(numTimes+2)*(nodeNum)+1);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end
if together, 
  set(gcf,'PaperPosition',0.65.*[0 0 12 5]);
else
  set(gcf,'PaperPosition',21.5.*[0 0 1.25 0.96*numNodes]);
end
