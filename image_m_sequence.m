function [I]=image_m_sequence(imageSize,imageBorder,gridSize,mseq,order,mseqIndex)
if rem(imageSize,gridSize)~=0, 
  disp(['ERROR image_m_sequence: ' ...
		'please make image size an exact multiple of the grid size']);
end

%create a sequence of images, in which each image is created such that each
%pixel contains samples, off-set by the constant, taken from the same m-sequence
p=2^order/(gridSize^2);
pixelsPerSquare=imageSize/gridSize;
lenSequence=length(mseq);

%create blank image with border that will surround checkerbord pattern
I=zeros(imageSize+2*imageBorder,'single')+0.5;
for locationX=1:gridSize
  for locationY=1:gridSize
	%fill in one square of the checkerboard with the value determined by
	%the m-sequence
	xStart=1+imageBorder+(locationX-1)*pixelsPerSquare;
	yStart=1+imageBorder+(locationY-1)*pixelsPerSquare;
	xEnd=xStart+pixelsPerSquare-1;
	yEnd=yStart+pixelsPerSquare-1;
	I(xStart:xEnd,yStart:yEnd)=...
		mseq(1+mod(mseqIndex+p*locationX+p*locationY*gridSize,lenSequence));
  end
end




