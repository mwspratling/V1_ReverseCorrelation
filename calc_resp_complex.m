function respComplex=calc_resp_complex(Y,x,y,features)
iterations=length(Y{1});
y_all_nodes=cat(1,Y{:});
%take max across features
respComplex(1:iterations)=max(y_all_nodes(features,:));
