function sigmaDist=plot_reverse_correl_orient_gaussfit(w,rcOrient,bin_labels,plotNodes,plotTimes)
if nargin<4
  numNodes=length(rcOrient);
else
  numNodes=length(plotNodes);
end
[poo,numTimes]=size(rcOrient{1});
sigmaDist=ones(length(rcOrient),numTimes).*NaN;
maxNodes=size(w,1);
maxTime=floor(numTimes/2);
if nargin<5
  plotTimes=[-maxTime:maxTime];
else
  numTimes=length(plotTimes);
end

numBins=size(rcOrient{1},1);
binsToLabel=[1:floor(numBins/2):numBins];
angle_values=[0:180/numBins:179];
clf

nodeNum=0;
for node=plotNodes
  nodeNum=nodeNum+1;
  %for each node show the the reverse correlation at each time lag
  rcOrientMax=max(max(rcOrient{node}))-min(min(rcOrient{node}));
  toff=0;
  for time=plotTimes
	toff=toff+1;
	data=rcOrient{node}(:,time+maxTime+1)';
	%data=data./sum(data); has no effect
	data=(data-min(data)).*10000000;
	data=rotate_data(data,node);%rotate orientation tuning curves so that
                                %orientation preference is approx central: this
                                %will allow a Gaussian to be fitted to the data.
	[A, mu, sigma, error] = fitgauss(data,angle_values);
	data=rotate_data_back(data,node);
	fitted_data=A*exp(-0.5*((angle_values-mu)./sigma).^2)/sqrt(2*pi*sigma^2);
	fitted_data=rotate_data_back(fitted_data,node);
	
	maxsubplot(numNodes+1,1+numTimes,(numTimes+1)*(nodeNum)+toff+1);
	plot([1:numBins],data), hold on
	plot([1:numBins],fitted_data,'r');
	axis([1,numBins,0,1.05*rcOrientMax.*10000000])
	axis('square')
	if sigma>0 && sigma<180 && error<0.2, 
	  title(num2str(sigma)), 
	  sigmaDist(node,time+maxTime+1)=sigma;
	end
	drawnow;
	if nodeNum==1
	  %set(gca,'XTick',angle_values,'XTickLabel',num2str(bin_labels')); 
	  set(gca,'XTick',binsToLabel,'XTickLabel',num2str(bin_labels(binsToLabel)')); 
	else
	  set(gca,'XTick',[]);
	end
	if toff~=1
	  set(gca,'YTick',[]);
	end
  end
  
  if node<=maxNodes & ~isempty(w)
	%show corresponding synaptic weigths
	RF=w{node,1}-w{node,2};
	lim=max(1e-9,0.9.*max(max(abs(RF))));
	maxsubplot(numNodes+1,1+numTimes,(numTimes+1)*(nodeNum)+1);
	imagesc(RF,[-lim,lim]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[]); drawnow;
  end
end
nanmean(sigmaDist,1)


function data=rotate_data(data,node)
dataOrig=data;
numBins=length(data);
numOris=8;
cent=5; %1st node with a central orientation preference
ori=mod(node-1,numOris)+1;
cut=fix((cent-ori)*numBins/numOris);
if cut>0
  range1=numBins-cut+1:numBins;
  range2=1:numBins-cut;	
elseif cut<0
  cut=abs(cut);
  range1=cut+1:numBins;
  range2=1:cut;
end	
if cut~=0  
  %centre data
  data(1:length(range1))=dataOrig(range1);
  data(length(range1)+1:numBins)=dataOrig(range2);
end

function data=rotate_data_back(data,node)
dataOrig=data;
numBins=length(data);
numOris=8;
cent=5; %1st node with a central orientation preference
ori=mod(node-1,numOris)+1;
cut=fix((cent-ori)*numBins/numOris);
if cut>0
  range1=numBins-cut+1:numBins;
  range2=1:numBins-cut;	
elseif cut<0
  cut=abs(cut);
  range1=cut+1:numBins;
  range2=1:cut;
end	
if cut~=0  
  %centre data
  data(range1)=dataOrig(1:length(range1));
  data(range2)=dataOrig(length(range1)+1:numBins);
end