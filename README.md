----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2012) Predictive coding accounts for V1 response 
properties recorded using reverse correlation,  Biological Cybernetics, 106(1):37-49.](https://nms.kcl.ac.uk/michael.spratling/Doc/v1_reverse_correl.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code was tested with MATLAB Version 7.10 (R2010a).

To use this software:
```
    run matlab 
    cd to the directory containing this code 
```
Some functions make use of the nansuite toolbox by Jan Glaescher, available
at http://www.mathworks.com/matlabcentral/fileexchange/6837 and/or the
fitgauss function by James R. Blake, available at http://www.mathworks.
co.uk/matlabcentral/fileexchange/7489-fitgauss. These functions should thus be
installed somewhere on your matlab path.


To perform any reverse correlation experiment edit the "imageType" parameter in
V1_reverse_correl.m to the appropriate value, then execute:
```
  >> [w,rcImage,rcFreq,rcOrient,rcBlank,rcJointOrient,fcStim]=V1_reverse_correl;
```

To analyse the results from an experiment Mapping Spatial RFs (ones with
imageType='sparse', 'hartley', or 'dense') use:
```
  >> plot_reverse_correl_image(w,rcImage,13,[1:8:32],[-2:6]);%Fig 2, Tables 1 and 2
```
Adjust the values in the 1st set of square brackets to select the nodes, and the
values in the 2nd set of square brackets to select the time-lags, at which the
recontructed RFs will be plotted.


To analyse the results from an experiment Measuring Orientation Tuning (ones
with imageType='CDL2005', 'RHS1997', or 'RHS2003') use:
```
  >> plot_reverse_correl_orient_logprob(w,rcOrient,rcBlank,[],[1:32],[-2:6],1,1);%Figs 3 and 4
```
or:
```
  >> plot_reverse_correl_orient(w,rcOrient,[],[29],[-1:6]);%Fig 5
```
In both cases, adjust the values in the 2nd set of square brackets to select the
nodes whose response is to be plotted, and the values in the 3rd set of square brackets to select the time-lags at which the tuning curves will be plotted.


To analyse the results from an experiment measuring the shift in preferred
orientation as a function of the orientation of the preceding stimulus  (i.e. one
with imageType='Felsen02') use:
```
  >> plot_orientation_tuning_shift(w,rcJointOrient,[1:32],[3]);%Fig 6
```
Adjust the values in the 1st set of square brackets to select the nodes, and the
value in the 2nd set of square brackets to select the time-lag, for which the
mean shift will be calculated.

